import { FrontmatterYearGroup, keyFormatter } from '../../utils/helpers';
import BlogCard from './BlogCard';
import styles from '../../styles/BlogYear.module.scss';

interface BlogYearProps {
  detail: FrontmatterYearGroup;
}

export default function BlogYear({ detail }: BlogYearProps) {
  const { year, frontmatters } = detail;
  return (
    <>
      <h4 className={styles.yearHeader}>{year}</h4>
      {frontmatters.map((frontmatter, i) => {
        return (
          <BlogCard
            key={keyFormatter(frontmatter.title, i)}
            frontmatter={frontmatter}
          />
        );
      })}
    </>
  );
}
