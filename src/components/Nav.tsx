import styles from '../../styles/Nav.module.scss';
import { MdClose } from 'react-icons/md';
import { FiMenu } from 'react-icons/fi';
import Link from 'next/link';
import React, { useState } from 'react';
import { FaGithub, FaLinkedin, FaMastodon } from 'react-icons/fa';

export default function Nav() {
  const [navbarOpen, setNavbarOpen] = useState(false);

  const handleToggle = () => {
    setNavbarOpen((prev) => !prev);
  };

  const closeMenu = () => {
    setNavbarOpen(false);
  };

  return (
    <nav className={styles.menu} data-testid="nav__main">
      <button
        className={`${styles.menuBtn} ${styles.btnLeft}`}
        onClick={closeMenu}
      >
        <Link href={'/'}>
          <a>
            <span className="first">p</span>
            <span className="second">k</span>
          </a>
        </Link>
      </button>
      <button
        className={`${styles.menuBtn} ${styles.btnRight} ${styles.hamburger}`}
        id="hamburger"
        title="Navigate this site"
        onClick={handleToggle}
      >
        {navbarOpen ? (
          <MdClose style={{ verticalAlign: '-0.45rem' }} />
        ) : (
          <FiMenu style={{ verticalAlign: '-0.45rem' }} />
        )}
      </button>
      <ul
        className={`${styles.menuListNormal} ${
          navbarOpen ? styles.showMenu : ''
        }`}
        id="nav-items"
      >
        <li
          className={`${styles.menuItem} ${
            navbarOpen ? '' : styles.horizontal
          }`}
        >
          <Link href={'/about'} data-testid="nav__now">
            <a className={styles.navItem} onClick={closeMenu}>
              about
            </a>
          </Link>
        </li>
        <li
          className={`${styles.menuItem} ${
            navbarOpen ? '' : styles.horizontal
          }`}
        >
          <Link href={'/blog'} data-testid="nav__projects">
            <a className={styles.navItem} onClick={closeMenu}>
              blog
            </a>
          </Link>
        </li>
        <div className={styles.mediaContainer}>
          <ul className={styles.mediaLinks}>
            <li className={styles.mediaItem}>
              <Link href="https://github.com/galambborong">
                <a className={styles.mediaItem}>
                  <FaGithub />
                </a>
              </Link>
            </li>
            <li className={styles.mediaItem}>
              <Link href="https://www.linkedin.com/in/p-keenan">
                <a className={styles.mediaItem}>
                  <FaLinkedin />
                </a>
              </Link>
            </li>
            <li className={styles.mediaItem}>
              <Link href="https://linuxrocks.online/@galambborong">
                <a className={styles.mediaItem}>
                  <FaMastodon />
                </a>
              </Link>
            </li>
          </ul>
        </div>
      </ul>
    </nav>
  );
}
