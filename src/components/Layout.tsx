import Nav from './Nav';
import React from 'react';
import Footer from './Footer';
import Head from 'next/head';
import { useRouter } from 'next/router';

export default function Layout({ children }: any) {
  const router = useRouter();

  return (
    <>
      <Head>
        <title>HOLDING TITLE</title>
        <meta name="description" content="HOLDING DESCRIPTION" />
        <meta
          property="og:url"
          content={`${router.basePath}${router.asPath}`}
        />
      </Head>
      <Nav />
      <main>{children}</main>
      <Footer />
    </>
  );
}
