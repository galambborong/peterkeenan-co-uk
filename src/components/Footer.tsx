import React, { useEffect, useState } from 'react';
import Link from 'next/link';
import { Commit, fetchLatestCommit } from '../../utils/githubFetchers';
import styles from '../../styles/Footer.module.scss';
import { FiGitCommit } from 'react-icons/fi';
import {
  FaGithub,
  FaLinkedin,
  FaMastodon,
  FaCreativeCommons,
  FaCreativeCommonsBy,
  FaCreativeCommonsSa
} from 'react-icons/fa';
import {
  convertDateTimeStringToDate,
  getDateDetails
} from '../../utils/helpers';

enum REQUEST_STATUS {
  SUCCESS,
  FAILURE,
  LOADING
}

export default function Footer() {
  const [commit, setCommit] = useState<Commit>({
    sha: '',
    url: '',
    date: ''
  });
  const [requestStatus, setRequestStatus] = useState(REQUEST_STATUS.LOADING);
  useEffect(() => {
    async function fetchAndSet() {
      try {
        const latestCommit = await fetchLatestCommit();
        setCommit(latestCommit);
        setRequestStatus(REQUEST_STATUS.SUCCESS);
      } catch (e) {
        setRequestStatus(REQUEST_STATUS.FAILURE);
      }
    }
    fetchAndSet();
  }, []);

  const { sha, url: html_url, date } = commit;

  const {
    month,
    date: day,
    year
  } = getDateDetails(convertDateTimeStringToDate(date));

  if (requestStatus === REQUEST_STATUS.LOADING) {
    return <div>LOADING</div>;
  }

  return (
    <footer className={styles.footer}>
      <div className={styles.commit}>
        <p>
          <FiGitCommit
            className={styles.commitSymbol}
            style={{
              fontSize: '1.5rem',
              verticalAlign: '-15%'
            }}
          />
          &ensp;
          <Link href={html_url}>
            <a className={styles.footerLink}>{sha}</a>
          </Link>
          <span className={styles.commitDate}>
            &ensp;
            {`${day} ${month} ${year}`}{' '}
          </span>
        </p>
      </div>
      <div className={styles.mediaContainer}>
        <ul className={styles.mediaLinks}>
          <li className={styles.mediaItem}>
            <Link href="https://github.com/galambborong">
              <a className={styles.mediaItem}>
                <FaGithub />
              </a>
            </Link>
          </li>
          <li className={styles.mediaItem}>
            <Link href="https://www.linkedin.com/in/p-keenan">
              <a className={styles.mediaItem}>
                <FaLinkedin />
              </a>
            </Link>
          </li>
          <li className={styles.mediaItem}>
            <Link href="https://linuxrocks.online/@galambborong">
              <a className={styles.mediaItem}>
                <FaMastodon />
              </a>
            </Link>
          </li>
        </ul>
      </div>
      <div className={styles.copyright} id="licence">
        <Link href="https://creativecommons.org/licenses/by-sa/4.0/">
          <a
            className={styles.creativeCommonsLink}
            title="Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)"
          >
            <FaCreativeCommons style={{ verticalAlign: 'middle' }} />{' '}
            <FaCreativeCommonsBy style={{ verticalAlign: 'middle' }} />{' '}
            <FaCreativeCommonsSa style={{ verticalAlign: 'middle' }} />
          </a>
        </Link>
      </div>
    </footer>
  );
}
