import sampleRes from './sampleRes';

export default {
  create: jest.fn().mockResolvedValue({
    url: ''
  }),
  get: jest.fn().mockResolvedValue({
    data: { sampleRes }
  }),
  fetchLatestCommit: jest.fn().mockResolvedValue({
    data: {}
  })
};
