import axios from 'axios';

export interface Commit {
  sha: string;
  url: string;
  date: string;
}

export const fetchLatestCommit: () => Promise<Commit> = async () => {
  const {
    data: { commit }
  } = await axios.get(
    'https://api.github.com/repos/galambborong/peterkeenan.co.uk/branches/main'
  );
  const { sha, html_url } = commit;
  const date = commit?.commit?.author?.date;
  return {
    sha: sha.substr(0, 7),
    url: html_url,
    date
  };
};
