---
title: Test post with much value and significance
createdAt: '2021-12-05T08:14:05.116Z'
blurb: This is an excerpt
slug: test-post
tags:
  - nextjs
  - reactjs
---

## Hello world

Hello, this is a test post.

- Item 1
- Item 2

Some content

```ts
const myVar = 'hello';
console.log();
```

```f#
let myFunc a b = a * b
```
