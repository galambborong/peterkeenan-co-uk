---
title: A fascinating article on an even more fascinating topic
subTitle: An even more fascinating subtitle on the same topic
blurb: This is a blurb for a document and hello there
slug: article-fascinating
createdAt: '2022-01-05T08:14:05.116Z'
lastModified: 17 November 2021
leadImage: /public/pexels-pixabay-270348.jpg
---

# H1 Header One

Hello, this is a test post.

- Item 1
- Item 2

## H2 Header Two

Here is a little content

1. With a numbered list
2. Of elements
3. To read

### H3 Header Three

```ts
const myVar = 'hello';
console.log(myVar);
```

```haskell
let myFunc a b = a * b
```

#### H4 Header Four

![This is an image](https://ik.imagekit.io/galambborong/pexels-pixabay-270348_Lwk2ItC99I8.jpg?tr=w-836)

> This is a quote

And some more normal text.

> And a really long quote which is likely to go on and on and on and on and on and on, for a long time. A very long time. This could even be a whole paragraph. It's a really long quote.

### Header Three H3

Does it work when I add code, like `this`? `camelCasedWord`

Or even `a multi-word code thingy`?
