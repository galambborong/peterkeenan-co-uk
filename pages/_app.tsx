import '../styles/main.scss';
import '../styles/prism.css';
import type { AppProps } from 'next/app';
import Layout from '../src/components/Layout';
import * as Fathom from 'fathom-client';
import { useRouter } from 'next/router';
import { useEffect } from 'react';

export default function MyApp({ Component, pageProps }: AppProps) {
  const router = useRouter();
  useEffect(() => {
    Fathom.load('SEATEUYQ', {
      includedDomains: [
        'happy-hypatia-e0cc84.netlify.app',
        'peterkeenan-co-uk.pages.dev'
      ]
    });

    function onRouteChangeComplete() {
      Fathom.trackPageview();
    }

    router.events.on('routeChangeComplete', onRouteChangeComplete);

    return () => {
      router.events.off('routeChangeComplete', onRouteChangeComplete);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return (
    <Layout>
      <Component {...pageProps} />
    </Layout>
  );
}
