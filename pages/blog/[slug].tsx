import { GetStaticPaths, GetStaticProps } from 'next';
import Header from '../../src/components/Header';
import {
  getMarkedUpContentAndFrontMatterByFileName,
  getFileNameBySlug,
  getSlugs,
  PostProps
} from '../../utils/dataFetchers';
import styles from '../../styles/BlogPost.module.scss';
import Prism from 'prismjs';
import { useEffect } from 'react';
import BlogBlurb from '../../src/components/BlogBlurb';
import {
  convertDateTimeStringToDate,
  getDateDetails,
  handleDate
} from '../../utils/helpers';

require('prismjs/components/prism-typescript');
require('prismjs/components/prism-fsharp');
require('prismjs/components/prism-rust');
require('prismjs/components/prism-haskell');

export default function Post({ content, frontmatter }: PostProps) {
  useEffect(() => {
    Prism.highlightAll();
  }, []);
  const { title, blurb, createdAt, subTitle, lastModified } = frontmatter;

  const createdAtDate = convertDateTimeStringToDate(createdAt);
  const createdAtInfo = createdAtDate.toDateString();

  const modifiedDateInformation = lastModified
    ? handleDate(lastModified)
    : null;

  return (
    <>
      <Header
        headerContent={{
          mainHeader: title,
          subHeader: subTitle ? subTitle : ''
        }}
      />
      <p>Published: {createdAtInfo}</p>
      {lastModified && <p>Updated: {lastModified}</p>}
      {blurb && <BlogBlurb blurb={blurb} />}
      <div
        className={styles.blogPostContainer}
        dangerouslySetInnerHTML={{ __html: content }}
      />
    </>
  );
}

export const getStaticPaths: GetStaticPaths = async () => {
  return {
    paths: getSlugs(),
    fallback: false
  };
};

export const getStaticProps: GetStaticProps<PostProps> = async ({
  params: { slug }
}: any) => {
  const matchingFile = getFileNameBySlug(slug);

  const { content, frontmatter } =
    getMarkedUpContentAndFrontMatterByFileName(matchingFile);

  return {
    props: {
      content,
      frontmatter
    }
  };
};
