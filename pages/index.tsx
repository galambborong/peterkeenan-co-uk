import styles from '../styles/Home.module.scss';
import Header from '../src/components/Header';
import { GiMokaPot } from 'react-icons/gi';
import { MdOutlineDirectionsBike, MdMusicNote } from 'react-icons/md';
import { BiCodeAlt } from 'react-icons/bi';

export default function Home() {
  const headerContent = {
    mainHeader: 'Ciao!',
    subHeader: "I'm Peter"
  };
  return (
    <>
      <Header headerContent={headerContent} />
      <p className={styles.heroContent}>
        I'm a developer and musician based in Newcastle, UK and this is my
        little corner of the web where I write about coding, music and coffee.
      </p>
      <div className={styles.heroIcons}>
        <GiMokaPot />
        <MdOutlineDirectionsBike />
        <MdMusicNote />
        <BiCodeAlt />
      </div>
    </>
  );
}
